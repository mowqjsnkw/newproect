package MyTest;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class annotations {
 
@BeforeSuite
public void beforesuite() {
	System.out.println("first annotation");
}
@AfterSuite
public void aftersuite() {
	System.out.println("second annotation");
}
@BeforeTest
public void BeforeTest() {
	System.out.println("third annotation");
}
@AfterTest
public void Aftertest() {
	System.out.println("four annotation");
}
@BeforeClass
public void BeforeClass() {
	System.out.println("five annotation");
}
@AfterClass
public void  AfterClass() {
	System.out.println("six annotation");
}
@Test
public void  TestClass() {
	System.out.println("seven annotation");
}
@BeforeMethod
public void  BeforeMethod() {
	System.out.println("eight annotation");
}
@AfterMethod
public void  AfterMethod() {
	System.out.println("nine annotation");
}
}
