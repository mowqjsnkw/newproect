package MyTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeSuite;
public class webbrowser{
@BeforeSuite
public void setBrowser(String browser){
   WebDriver driver;
if (browser.equalsIgnoreCase("Firefox")) {
    driver = new FirefoxDriver();  
   }
   else if (browser.equalsIgnoreCase("Chrome")) {  
	System.setProperty("webdriver.chrome.driver", 
		         "C:\\Users\\Maheswari.p\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		      driver = new ChromeDriver();
      driver = new ChromeDriver();
   }
   else {
      throw new IllegalArgumentException("Invalid browser value!!");
   }
   driver.manage().window().maximize();
}
}